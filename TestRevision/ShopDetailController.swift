//
//  ShopDetailController.swift
//  TestRevision
//
//  Created by User on 26/11/2019.
//  Copyright © 2019 Pong. All rights reserved.
//

import UIKit
import MapKit

class ShopDetailController: UIViewController{
    
    var shop: Shop?
    var anntations = [MKPointAnnotation]()

    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var shopLocation: UILabel!
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shopName.text = "Shop Name: \((shop?.title)!)"
        shopLocation.text = "Shop Location: \((shop?.lat)!), \((shop?.lng)!)"
        
        let nycAnnotation = MKPointAnnotation()
        nycAnnotation.coordinate = CLLocationCoordinate2D(latitude: (shop?.lat)!, longitude: (shop?.lng)!)
        nycAnnotation.title = shop?.title
        self.anntations.append(nycAnnotation)
        self.map.addAnnotations(self.anntations)
        
        let zoom = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let coord = nycAnnotation.coordinate
        let region = MKCoordinateRegion(center: coord, span: zoom)
        self.map.setRegion(region, animated: false)

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
