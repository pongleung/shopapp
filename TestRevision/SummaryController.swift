//
//  SummaryController.swift
//  TestRevision
//
//  Created by User on 26/11/2019.
//  Copyright © 2019 Pong. All rights reserved.
//

import UIKit
import MapKit

class SummaryController: UIViewController {
    var dataController : DataController?
    
    @IBOutlet weak var Msg: UILabel!
    @IBOutlet weak var map: MKMapView!
    var anntations = [MKPointAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Msg.text = "You have \((dataController?.listLenght())!) shops"
        
        dataController?.dataArray.forEach({ (shop) in
            let nycAnnotation = MKPointAnnotation()
            nycAnnotation.coordinate = CLLocationCoordinate2D(latitude: shop.lat, longitude: shop.lng)
            nycAnnotation.title = shop.title
            self.anntations.append(nycAnnotation)
            self.map.addAnnotations(self.anntations)
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
