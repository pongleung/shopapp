//
//  Shop.swift
//  TestRevision
//
//  Created by User on 26/11/2019.
//  Copyright © 2019 Pong. All rights reserved.
//

import Foundation

class Shop {
    var title: String
    var lat: Double
    var lng: Double
    init(title: String, lat: Double, lng: Double) {
        self.title = title
        self.lat = lat
        self.lng = lng
    }
}

class DataController{
    var dataArray : [Shop] = []
    
    init(){
        let shop1 = Shop(title: "Shatin", lat: 22.381, lng: 114.189)
        let shop2 = Shop(title: "Tai Po", lat: 22.449, lng: 114.171)
        
        self.dataArray.append(shop1)
        self.dataArray.append(shop2)
    }
    
    func listLenght() -> Int{
        return dataArray.count
    }
    
    func shopAtIndex(idx: Int) -> Shop{
        return dataArray[idx]
    }
    
    func addShop(shop: Shop) {
        dataArray.append(shop)
    }
    
    func delShop(_ idx: Int){
        dataArray.remove(at: idx)
    }
}
